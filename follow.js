var table = $('table tr');
var seenDisplay = $('.met-character')
const WIDTH = 4, HEIGHT = 4
var playing = false, turns = 0, seen = 0;
// VacantCell is the last cell the train has moved forward out of; spawning
// clears this, and no further spawning can occur until the tail of the train
// moves forward
var vacantCell = null;
// charQueue is an array, each element is [avatar number, [row, column]]
var charQueue = [[0,null]]

// 0 = fnibbit  1 = herp derp 2 = OMG BACON!! 3 = zany serendipity 4 = gertie mack
// 5= glum pudding 6 = gilbert whisper -- glum and gilbert not yet implemented
// it's a hash so images can be added for facing different directions; f is for
// forward, actually right-facing.
// Glitch spritesheets: angry is 20 images, 128 tall by 94 wide.
// base is 15 images, 127 tall by 98 wide << first 12, right; mirror image for left
// climb is 19 images, 121 x 106 << up
// jump is 33 images, 131 x 116; << 18th-26th are good for down.
var characters = {
  0: {'f': "https://d3tv7e6jdw6zbr.cloudfront.net/avatars/2012-07-02/1db738238afb4f120a670f18b630f9b0_1341249345_100_flip.png"},
  1: {'f':"https://d3tv7e6jdw6zbr.cloudfront.net/avatars/2012-02-15/7d846950b83cfd33037429f2d115c19e_1329321966_100_flip.png"},
  2: {'f':"https://d3tv7e6jdw6zbr.cloudfront.net/avatars/2013-03-05/dbe2f3adb8b3be17aef90ef9146e3070_1362543571_100_flip.png"},
  3: {'f':"https://d3tv7e6jdw6zbr.cloudfront.net/avatars/2012-12-07/c4d9e2aa4524f92ea2283291ee9a3c46_1354940352_100_flip.png"},
  4: {'f':"https://d3tv7e6jdw6zbr.cloudfront.net/avatars/2012-09-13/b60dcf4002f4167dc3fe3100c4f6fded_1347572252_100_flip.png"},
  5: {'f':"https://d3tv7e6jdw6zbr.cloudfront.net/avatars/2012-09-22/3aa0a47c0b2ff6747a4bd0f14d8919b3_1348328164_100_flip.png"},
  6: {'f':"https://d3tv7e6jdw6zbr.cloudfront.net/avatars/2012-10-31/5559000404d01002138259fa7f84bd29_1351738788_100_flip.png"}
}


function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
function clearSeen(){
  // clear the display of seen characters when the game resets
  var i;
  for (i = 0; i < seenDisplay.length; i++) {
    seenDisplay.eq(i).html("");
  }
}
function addSeen(){
  // When I jostle up a new character, add it to the display of new characters
  // right now I have a repertoire of 5 characters
  if (seen < 7) {
    seenDisplay.eq(seen).html("<img src=\"" +
      characters[seen]['f'] + "\">")
    seen += 1;
  }
}
function reportEnd(message) {
  alert(message +"\nGame over!\nYou led for " + turns + " turns and met "
    + seen + " characters.");
  playing=false;
  clearSeen();
  resetGrid();
}

function resetGrid() {
  turns = 0;
  seen = 1;
  var i,j;
  for (i = 0; i<HEIGHT; i++) {
    for (j = 0; j < WIDTH; j++) {
      exitCell(i,j);
    }
  }
  vacantCell = null;
  charQueue = [[0,null]]
}


function enterCell(rowIndex,colIndex,character) {
  return table.eq(rowIndex).find('td').eq(colIndex).html("<img src=\"" +
    characters[character]['f'] + "\">")
}

function exitCell(rowIndex, colIndex) {
  vacantCell = [rowIndex, colIndex]
  return clearCell(rowIndex, colIndex);
}

function clearCell(rowIndex, colIndex) {
    return table.eq(rowIndex).find('td').eq(colIndex).html("");
}

function spawn() {
  var finder, i, newChar = 0, ceiling = Math.pow(2,seen);
  if (Math.random() > 0.66) {
    if (vacantCell != null) {
      console.log("seen " + seen + " ceiling " + ceiling)
      finder = Math.floor(Math.random()*ceiling);
      for (i = seen - 1; i >= 0; i--) {
        if (finder > Math.pow(2, seen-i-1)) {
          newChar = i;
        }
      }
      charQueue.push([newChar, vacantCell]);
      enterCell(vacantCell[0], vacantCell[1], newChar);
      vacantCell = null;
    }
    else {
      reportEnd("New character tried to join train but didn't know where to grab on.");
    }
  }
}
function jostle() {
  var i, current, nextRow, nextCol, r, c, merged = false;
  if (charQueue.length > 1) {
    for (i = 1; i < charQueue.length; i++){
      if (charQueue[i-1][0] === charQueue[i][0]) {
        current = charQueue[i-1];
        if (current[0] + 1 < Object.keys(characters).length) {
          current[0] = current[0] + 1;
        }
        if (current[0] >= seen) {
          addSeen();
        }
        enterCell(current[1][0], current[1][1], current[0])
        r = charQueue[i][1][0];
        c = charQueue[i][1][1];
        charQueue.splice(i, 1);
        for (; i < charQueue.length; i++) {
          current = charQueue[i];
          nextRow = current[1][0];
          nextCol = current[1][1]
          current[1] = [r, c];
          enterCell(r, c, current[0]);
          clearCell(nextRow, nextCol)
          r = nextRow;
          c = nextCol;
        }
        exitCell(r,c)
      }
    }
  }
  turns++
}

function move(keycode) {
  var current = charQueue[0][1], moved = false;
  if (playing){
    switch(keycode) {

      case 37:
        if (current[1] > 0) {
          moveTrain(current[0], current[1] - 1)
        }
        else {
          jostle();
        }
        moved = true;
        break;
      case 38:
        if (current[0] > 0) {
          moveTrain(current[0] - 1, current[1]);
        }
        else {
          jostle();
        }
        moved = true;
        break;
      case 39:
        if (current[1] < WIDTH - 1) {
          moveTrain(current[0], current[1] + 1)
        }
        else {
          jostle();
        }
        moved = true;
        break;
      case 40:
        if (current[0] < HEIGHT - 1) {
          moveTrain(current[0] + 1, current[1])
        }
        else {
          jostle();
        }
        moved = true;
        break;
      case 32:
        jostle();
        moved = true;
      }
      if (moved && playing) {
        spawn();
        turns++;
      }
    }
  }

function logQueue(){
  var i, message = "";
  for (i=0; i<charQueue.length; i++){
    message += charQueue[i][0] + ", ";
  }
  console.log(message);
}
function moveTrain(rowIndex, colIndex) {
  var i, current, r = rowIndex, c = colIndex, nextRow, nextCol;
  for (i = 0; i < charQueue.length; i++) {
    if (charQueue[i][1][0] == rowIndex && charQueue[i][1][1] == colIndex) {
      reportEnd(`Collision at ${rowIndex}, ${colIndex}.`);
      return;
    }
  }
  for (i = 0; i < charQueue.length; i++) {
    current = charQueue[i];
    nextRow = current[1][0];
    nextCol = current[1][1]
    current[1] = [r, c];
    enterCell(r, c, current[0]);
    clearCell(nextRow, nextCol)
    r = nextRow;
    c = nextCol;
  }
  exitCell(r,c);
}

function startGame(rowIndex, colIndex) {
  playing = true;
  enterCell(rowIndex, colIndex, 0);
  charQueue[0][1] = [rowIndex, colIndex];
  turns = 1;
  seen = 0;
  addSeen()
}
$(document).ready(function(){
  $('.board td').on('click', function(){
    var col = $(this).index();
    var row = $(this).closest("tr").index();
    if (!playing){
      startGame(row,col)
    }
    else {console.log("Clicked on board mid-game. " + row + ", " + col)}
  })

  // use jquery .keydown(), bind it to the document.  Use the .which property of the event
  // left = 37 up = 38 right = 39 down = 40 space = 32.  wasd  is 87 65 83 68

  $(document).keydown(function(event) {
    if ((36 < event.which < 41) || event.which == 32) {
      move(event.which);
    }
  })
  $('.btn-info').on('click', function(){
    alert("Click on the game board to start. Use the arrow keys to move around. Sometimes a friend will turn up to follow you. Hit space to wait a moment; your friends will jostle each other as they wait.")
  })
  $('.btn-danger').on('click', function(){
    reportEnd("Resetting.")
  })
})
